use day1::*;
fn main() {
    let input = std::fs::read_to_string("input.txt").expect("error reading file");
    let numbers = convert_input(input);
    let windowed = sliding_window(numbers.clone());
    let number_of_increases = count_increases(numbers);
    let number_of_increases_windowed = count_increases(windowed);
    println!("normal: {}, windowed: {}", number_of_increases, number_of_increases_windowed)
}
