use super::*;

#[test]
fn test_example_input() {
    let input = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
    let expected_answer = 7;

    let answer = count_increases(input);
    assert_eq!(expected_answer, answer);
}

#[test]
fn test_conversion() {
  let input = String::from(r#"199
200
208
210
200
207
240
269
260
263"#);
  let expected_answer = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];

  let answer = convert_input(input);
  assert_eq!(expected_answer, answer);
}

#[test]
fn test_sliding_window() {
    let input = vec![1, 2, 3, 4, 5, 6];
    let expected_answer = vec![6, 9, 12, 15];
    let answer = sliding_window(input);
    assert_eq!(expected_answer.len(), answer.len());
    assert_eq!(expected_answer, answer);
}

#[test]
fn test_sliding_window_example() {

  let input = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
  let expected_answer = 5;

  let windowed = sliding_window(input);
  println!("{:?}", windowed);
  let answer = count_increases(windowed);
  assert_eq!(expected_answer, answer);
}