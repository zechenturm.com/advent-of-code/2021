#[cfg(test)]
mod test;

use std::collections::VecDeque;

pub fn count_increases(input: Vec<i64>) -> i64 {
  let mut last: Option<i64> = None;
  let mut number_of_increases = 0;
  for current in input.iter() {
    if let Some(last) = last {
      if current > &last {
        number_of_increases += 1;
      }
    }
    last = Some(*current);
  }
  number_of_increases
}

pub fn convert_input(input: String) -> Vec<i64> {
  let mut numbers = vec![];
  for line in input.lines() {
    let number = line.parse().expect("Error parsing number");
    numbers.push(number);
  }
  numbers
}

pub fn sliding_window(input: Vec<i64>) -> Vec<i64> {
  let mut last = VecDeque::from([input[1], input[0]]);
  let mut output = Vec::new();
  for number in input.iter().skip(2) {
    let sum = number + last[0] + last.pop_back().expect("empty window");
    last.push_front(*number);
    output.push(sum);
  }
  output
}